import { Module } from '@nestjs/common';
import { RecipeIngredientsService } from './recipe-ingredients.service';
import { RecipeIngredientsController } from './recipe-ingredients.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
  RecipeIngredient,
  RecipeIngredientSchema,
} from './entities/recipe-ingredient.entity';
import { IngredientsService } from 'src/ingredients/ingredients.service';
import {
  Ingredient,
  IngredientSchema,
} from 'src/ingredients/entities/ingredient.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: RecipeIngredient.name, schema: RecipeIngredientSchema },
      { name: Ingredient.name, schema: IngredientSchema },
    ]),
  ],
  controllers: [RecipeIngredientsController],
  providers: [RecipeIngredientsService, IngredientsService],
})
export class RecipeIngredientsModule {}
