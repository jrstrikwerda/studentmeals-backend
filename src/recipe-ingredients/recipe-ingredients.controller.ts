import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { RecipeIngredientsService } from './recipe-ingredients.service';
import { CreateRecipeIngredientDto } from './dto/create-recipe-ingredient.dto';
import { UpdateRecipeIngredientDto } from './dto/update-recipe-ingredient.dto';
import { RecipeIngredient } from './entities/recipe-ingredient.entity';
import { Roles } from 'src/auth/roles/roles.decorator';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';
// import RoleGuard from 'src/common/guards/role.guard';
import { Role } from 'src/users/dto/user.dto';
import { Types } from 'mongoose';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('recipe-ingredients')
@Controller('recipe-ingredients')
export class RecipeIngredientsController {
  constructor(
    private readonly recipeIngredientsService: RecipeIngredientsService,
  ) {}

  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Post()
  create(
    @Param('recipeId') recipeId: Types.ObjectId,
    @Body() createRecipeIngredientDto: CreateRecipeIngredientDto,
  ): Promise<RecipeIngredient> {
    return this.recipeIngredientsService.createRecipeIngredient(
      createRecipeIngredientDto,
      recipeId,
    );
  }

  @Get()
  findAll(): Promise<RecipeIngredient[]> {
    return this.recipeIngredientsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<RecipeIngredient> {
    return this.recipeIngredientsService.findOne(id);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRecipeIngredientDto: UpdateRecipeIngredientDto,
  ): Promise<RecipeIngredient> {
    return this.recipeIngredientsService.update(id, updateRecipeIngredientDto);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Delete(':id')
  remove(@Param('id') id: string): Promise<RecipeIngredient> {
    return this.recipeIngredientsService.remove(id);
  }
}
