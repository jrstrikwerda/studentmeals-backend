import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsString, IsNumber } from 'class-validator';
import { IngredientDto } from 'src/ingredients/dto/ingredient.dto';
import { RecipeDto } from 'src/recipes/dto/recipe.dto';

export class RecipeIngredientDto {
  constructor(partial?: Partial<RecipeIngredientDto>) {
    if (partial) {
      Object.assign(this, partial);
    }
  }

  @ApiPropertyOptional()
  @IsNumber()
  amount?: number;

  @ApiPropertyOptional()
  @IsString()
  measurement?: string;

  @ApiPropertyOptional()
  @IsString()
  usage?: string;

  @ApiProperty()
  ingredient: IngredientDto;

  @ApiProperty()
  recipe: RecipeDto;
}
