import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IngredientDto } from 'src/ingredients/dto/ingredient.dto';

export class CreateRecipeIngredientDto {
  @ApiPropertyOptional()
  measurement?: string;
  @ApiProperty()
  ingredient: IngredientDto;
  @ApiPropertyOptional()
  amount?: number;
  @ApiPropertyOptional()
  usage?: string;
}
