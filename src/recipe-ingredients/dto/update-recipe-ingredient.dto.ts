import { RecipeIngredientDto } from './recipe-ingredient.dto';

export class UpdateRecipeIngredientDto extends RecipeIngredientDto {}
