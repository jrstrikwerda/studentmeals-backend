import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';
import { IngredientsService } from 'src/ingredients/ingredients.service';
import { CreateRecipeIngredientDto } from './dto/create-recipe-ingredient.dto';
import { UpdateRecipeIngredientDto } from './dto/update-recipe-ingredient.dto';
import {
  RecipeIngredient,
  RecipeIngredientDocument,
} from './entities/recipe-ingredient.entity';

@Injectable()
export class RecipeIngredientsService {
  constructor(
    @InjectModel(RecipeIngredient.name)
    private recipeIngredientModel: Model<RecipeIngredientDocument>,
    @InjectModel(Ingredient.name)
    private ingredientModel: Model<Ingredient>,
    private ingredientsService: IngredientsService,
  ) {}
  async createRecipeIngredient(
    createRecipeIngredientDto: CreateRecipeIngredientDto,
    recipeId?: Types.ObjectId,
  ): Promise<RecipeIngredientDocument> {
    createRecipeIngredientDto.amount = createRecipeIngredientDto.amount || 0;
    createRecipeIngredientDto.measurement =
      createRecipeIngredientDto.measurement || '';
    createRecipeIngredientDto.usage = createRecipeIngredientDto.usage || '';

    try {
      // Get or create the ingredient
      const ingredient = await this.ingredientsService.createIngredient(
        createRecipeIngredientDto.ingredient,
        recipeId,
      );

      // Check if a recipe ingredient with the same values already exists
      const existingRecipeIngredient = await this.recipeIngredientModel
        .findOne({
          ...createRecipeIngredientDto,
          ingredient: ingredient,
        })
        .exec();

      // If a recipe ingredient with the same values already exists, return it
      if (existingRecipeIngredient) {
        return existingRecipeIngredient;
      }

      // requires check for existing recipe ingredient.

      const createdRecipeIngredient = await this.recipeIngredientModel.create({
        ...createRecipeIngredientDto,
        ingredient: ingredient,
        recipeId,
      });

      // createdRecipeIngredient.ingredient = ingredient._id;
      // createdRecipeIngredient.save();

      return await this.recipeIngredientModel
        .findById(createdRecipeIngredient.id)
        .populate({
          path: 'ingredient',
          model: 'Ingredient',
          select: { recipes: 0 },
        })
        .exec();
    } catch (error) {
      throw new InternalServerErrorException(
        `Error while creating a recipe ingredient: ${error.message}`,
      );
    }
  }

  async findAll(): Promise<RecipeIngredientDocument[]> {
    return this.recipeIngredientModel
      .find({})
      .populate({
        path: 'ingredient',
        model: 'Ingredient',
      })
      .exec();
  }

  async findOne(id: string): Promise<RecipeIngredientDocument> {
    return await this.recipeIngredientModel.findById(id).exec();
  }

  async update(
    id: string,
    updateRecipeIngredientDto: UpdateRecipeIngredientDto,
  ) {
    const recipeIngredient = await this.recipeIngredientModel
      .findById(id)
      .exec();
    await recipeIngredient.updateOne(updateRecipeIngredientDto).exec();
    return this.recipeIngredientModel.findById(id).exec();
  }

  async updateMany(condition: object, update: object): Promise<void> {
    await this.recipeIngredientModel.updateMany(condition, update);
  }

  async remove(id: string): Promise<RecipeIngredientDocument> {
    return this.recipeIngredientModel.findByIdAndDelete(id).exec();
  }
}
