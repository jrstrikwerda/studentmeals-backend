import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

export type RecipeIngredientDocument = RecipeIngredient & Document;

@Schema()
export class RecipeIngredient {
  @Prop()
  amount: number;

  @Prop()
  measurement: string;

  @Prop()
  usage: string;

  @Prop({ type: SchemaTypes.ObjectId, ref: 'Ingredient' })
  ingredient: Types.ObjectId;

  @Prop({ type: SchemaTypes.ObjectId, ref: 'Recipe' })
  recipe: Types.ObjectId;
}

export const RecipeIngredientSchema =
  SchemaFactory.createForClass(RecipeIngredient);
