import { SetMetadata } from '@nestjs/common';
import { Role } from 'src/users/dto/user.dto';

export const Roles = (...roles: Role[]) => SetMetadata('roles', roles);
