import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  // Res,
  UseGuards,
  Headers,
} from '@nestjs/common';
import { Request } from 'express';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';
import { RefreshTokenGuard } from 'src/common/guards/refreshToken.guard';
import { LocalAuthGuard } from 'src/common/guards/localAuth.guard';
// import { Response as ResponseType } from 'express';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Request as RequestType } from 'express';
@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  register(@Body() createUserDto: CreateUserDto) {
    return this.authService.register(createUserDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Body() req: AuthDto) {
    const data = await this.authService.login(req);
    // this.authService.storeTokenInCookie(res, data.tokens);
    // res.status(200).send({ message: 'ok' });
    const safeUser = {
      firstName: data.user.firstName,
      lastName: data.user.lastName,
      username: data.user.username,
      role: data.user.role,
      tokens: data.tokens,
    };
    return safeUser;
  }

  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Get('logout')
  logout(@Req() req: Request) {
    this.authService.logout(req.user['sub']);
  }

  @UseGuards(RefreshTokenGuard)
  @Get('refresh')
  @ApiQuery({ name: 'username' })
  async refreshTokens(
    @Query() query,
    @Req() req: RequestType,
    @Headers('authorization') refreshToken: string,
    // @Res({ passthrough: true }) res: ResponseType,
  ) {
    // const refreshToken = req.cookies.refresh_token;
    const newAuthToken = await this.authService.refreshTokens(
      query.username,
      refreshToken.slice(7),
    );
    // this.authService.storeTokenInCookie(res, newAuthToken);
    // res.status(200).send({ message: 'ok' });
    return newAuthToken;
  }

  // @UseGuards(RefreshTokenGuard)
  // @Get('refresh')
  // @ApiQuery({ name: 'username' })
  // async refreshTokens(
  //   @Query() query,
  //   @Req() req: RequestType,
  //   // @Res({ passthrough: true }) res: ResponseType,
  // ) {
  //   const refreshToken = req.cookies.refresh_token;
  //   const newAuthToken = await this.authService.refreshTokens(
  //     query.username,
  //     refreshToken,
  //   );
  //   // this.authService.storeTokenInCookie(res, newAuthToken);
  //   // res.status(200).send({ message: 'ok' });
  //   return newAuthToken;
  // }
}
