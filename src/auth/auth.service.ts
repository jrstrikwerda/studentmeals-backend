import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotAcceptableException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { ConfigService } from '@nestjs/config';
import { AuthDto, AuthTokenDto } from './dto/auth.dto';
import { Response as ResponseType } from 'express';
import { UserDto } from 'src/users/dto/user.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  //   async register(
  //     @Body('password') password: string,
  //     @Body('username') username: string,
  //   ): Promise<UserDocument> {
  //     const saltOrRounds = 10;
  //     const hashedPassword = await bcrypt.hash(password, saltOrRounds);
  //     const result = await this.usersService.create(username, hashedPassword);
  //     return result;
  //   }

  async register(createUserDto: CreateUserDto): Promise<any> {
    // Check if user exists
    const userExists = await this.usersService.findOneByUsername(
      createUserDto.username,
    );
    if (userExists) {
      throw new BadRequestException('User already exists');
    }

    // Hash password
    const hash = await this.hashData(createUserDto.password, 10);
    const newUser = await this.usersService.create({
      ...createUserDto,
      password: hash,
    });
    const tokens = await this.getTokens(
      newUser._id,
      newUser.username,
      newUser.role,
    );
    await this.updateRefreshToken(newUser._id, tokens.refreshToken);
    return tokens;
  }

  async login(
    data: AuthDto,
  ): Promise<{ tokens: AuthTokenDto; user: Partial<UserDto> }> {
    // Check if user exists
    const user = await this.usersService.findOneByUsernamePrivate(
      data.username,
    );
    if (!user) throw new NotAcceptableException('User not found');
    const passwordMatches = await bcrypt.compare(data.password, user.password);
    if (!passwordMatches)
      throw new NotAcceptableException('Incorrect password');
    const tokens = await this.getTokens(user._id, user.username, user.role);
    await this.updateRefreshToken(user._id, tokens.refreshToken);
    return { tokens, user };
  }

  async logout(userId: string) {
    return this.usersService.update(userId, { refreshToken: null });
  }

  hashData(data: string, saltOrRounds: number) {
    return bcrypt.hash(data, saltOrRounds);
  }

  async updateRefreshToken(userId: string, refreshToken: string) {
    const hashedRefreshToken = await this.hashData(refreshToken, 10);
    await this.usersService.update(userId, {
      refreshToken: hashedRefreshToken,
    });
  }

  storeTokenInCookie(res: ResponseType, authToken: AuthTokenDto) {
    res.cookie('access_token', authToken.accessToken, {
      maxAge: 1000 * 60 * 60 * 24,
      httpOnly: true,
    });
    res.cookie('refresh_token', authToken.refreshToken, {
      maxAge: 1000 * 60 * 60 * 24 * 7,
      httpOnly: true,
    });
  }

  async getTokens(
    userId: string,
    username: string,
    role: string,
  ): Promise<AuthTokenDto> {
    const accessTokenExpiration = Date.now() + 24 * 60 * 60 * 1000; // set expiry to 24 hours from now

    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        {
          sub: userId,
          username,
          role,
        },
        {
          secret: this.configService.get<string>('JWT_ACCESS_SECRET'),
          expiresIn: '24h',
        },
      ),
      this.jwtService.signAsync(
        {
          sub: userId,
          username,
          role,
        },
        {
          secret: this.configService.get<string>('JWT_ACCESS_SECRET'),
          expiresIn: '7d',
        },
      ),
    ]);

    return {
      accessToken,
      refreshToken,
      accessTokenExpiration,
    };
  }

  async refreshTokens(username: string, refreshToken: string) {
    const user = await this.usersService.findOneByUsernamePrivate(username);
    if (!user || !user.refreshToken)
      throw new ForbiddenException('Access Denied');
    const refreshTokenMatches = await bcrypt.compare(
      refreshToken,
      user.refreshToken,
    );
    if (!refreshTokenMatches) throw new ForbiddenException('Access Denied');
    const tokens = await this.getTokens(user.id, user.username, user.role);
    await this.updateRefreshToken(user.id, tokens.refreshToken);
    return tokens;
  }
}
