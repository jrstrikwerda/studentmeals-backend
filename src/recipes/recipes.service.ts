import {
  Injectable,
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IngredientsService } from 'src/ingredients/ingredients.service';
import { RecipeIngredientsService } from 'src/recipe-ingredients/recipe-ingredients.service';
import { RecipeStepsService } from 'src/recipe-step/recipe-steps.service';
import { CreateRecipeDto } from './dto/create-recipe.dto';
import { UpdateRecipeDto } from './dto/update-recipe.dto';
import { Recipe, RecipeDocument } from './entities/recipe.entity';

@Injectable()
export class RecipesService {
  constructor(
    @InjectModel(Recipe.name) private recipeModel: Model<RecipeDocument>,
    private recipeIngredientsService: RecipeIngredientsService,
    private ingredientService: IngredientsService,
    private recipeStepsService: RecipeStepsService,
  ) {}

  async createRecipe(
    createRecipeDto: CreateRecipeDto,
  ): Promise<RecipeDocument> {
    // Check if no data was posted
    if (!createRecipeDto || !Object.keys(createRecipeDto).length) {
      throw new BadRequestException('No data was posted');
    }

    try {
      // Check if a recipe with the same name already exists
      const existingRecipe = await this.recipeModel
        .findOne({
          name: createRecipeDto.name,
        })
        .populate({
          path: 'recipeIngredients',
          model: 'RecipeIngredient',
          populate: {
            path: 'ingredient',
            model: 'Ingredient',
            select: { recipes: 0 },
          },
        })
        .populate({
          path: 'recipeSteps',
          model: 'RecipeStep',
          select: { recipe: 0 },
        })
        .exec();

      // If a recipe with the same name already exists, return it
      if (existingRecipe) {
        return existingRecipe;
      }

      // Validate that required fields are present
      if (!createRecipeDto.name) {
        throw new BadRequestException('Recipe name is required');
      }
      if (
        !createRecipeDto.recipeIngredients ||
        !createRecipeDto.recipeIngredients.length
      ) {
        throw new BadRequestException(
          'At least one recipe ingredient is required',
        );
      }
      if (!createRecipeDto.recipeSteps || !createRecipeDto.recipeSteps.length) {
        throw new BadRequestException('At least one recipe step is required');
      }

      // Set default values for fields if they are not provided
      const date = new Date().toISOString();
      createRecipeDto.creationDate = createRecipeDto.creationDate || date;
      createRecipeDto.updatedDate = createRecipeDto.updatedDate || date;
      createRecipeDto.cookTime = createRecipeDto.cookTime || 0;
      createRecipeDto.description = createRecipeDto.description || '';
      createRecipeDto.image = createRecipeDto.image || '/noImage.png';
      createRecipeDto.recipeIngredients =
        createRecipeDto.recipeIngredients || [];
      createRecipeDto.recipeSteps = createRecipeDto.recipeSteps || [];

      // create all ingredients for the recipe
      const recipeIngredients = createRecipeDto.recipeIngredients.map(
        async (recipeIngr) => {
          return this.recipeIngredientsService.createRecipeIngredient(
            recipeIngr,
          );
        },
      );

      // Wait for all ingredients to be created/retrieved
      const createdRecipeIngredients = await Promise.all(recipeIngredients);

      // create all steps for the recipe
      const recipeSteps = createRecipeDto.recipeSteps.map(
        async (recipeStep) => {
          return this.recipeStepsService.createRecipeStep(recipeStep);
        },
      );

      // Wait for all steps to be created/retrieved
      const createdRecipeSteps = await Promise.all(recipeSteps);

      // Use the ingredientIds when creating the recipe
      const newRecipe = await this.recipeModel.create({
        ...createRecipeDto,
        recipeIngredients: createdRecipeIngredients,
        recipeSteps: createdRecipeSteps,
      });

      // Update the recipeIngredients to associate them with the new recipe
      await this.recipeIngredientsService.updateMany(
        {
          _id: {
            $in: createdRecipeIngredients.map((ri) => ri._id),
          },
        },
        { recipe: newRecipe._id },
      );

      // Update the ingredients in the recipeIngredients to associate them with the new recipe
      await this.ingredientService.updateMany(
        { _id: { $in: createdRecipeIngredients.map((ri) => ri.ingredient) } },
        { $addToSet: { recipes: newRecipe._id } },
      );

      // Update the recipeSteps to associate them with the new recipe
      await this.recipeStepsService.updateMany(
        {
          _id: { $in: createdRecipeSteps.map((s) => s._id) },
        },
        { recipe: newRecipe._id },
      );

      // Populate the referenced data
      return await this.recipeModel
        .findById(newRecipe._id)
        .populate({
          path: 'recipeIngredients',
          model: 'RecipeIngredient',
          populate: {
            path: 'ingredient',
            model: 'Ingredient',
            select: { recipes: 0 },
          },
        })
        .populate({
          path: 'recipeSteps',
          model: 'RecipeStep',
          select: { recipe: 0 },
        })
        .exec();
    } catch (error) {
      throw new InternalServerErrorException(
        `Error while creating a recipe: ${error.message}`,
      );
    }
  }

  async findAll(): Promise<RecipeDocument[]> {
    // Find all recipes and populate the related ingredients
    try {
      return await this.recipeModel
        .find({})
        .populate({
          path: 'recipeIngredients',
          model: 'RecipeIngredient',
          populate: {
            path: 'ingredient',
            model: 'Ingredient',
            select: { recipes: 0 },
          },
        })
        .populate({
          path: 'recipeSteps',
          model: 'RecipeStep',
          select: { recipe: 0 },
        })
        .exec();
    } catch (error) {
      throw new Error(`Error while fetching recipes: ${error}`);
    }
  }

  // Find a recipe by its id and populate the related ingredients
  async findOne(id: string): Promise<RecipeDocument> {
    try {
      return await this.recipeModel
        .findById(id)
        .populate({
          path: 'recipeIngredients',
          model: 'RecipeIngredient',
          populate: {
            path: 'ingredient',
            model: 'Ingredient',
            select: { recipes: 0 },
          },
        })
        .populate({
          path: 'recipeSteps',
          model: 'RecipeStep',
          select: { recipe: 0 },
        })
        .exec();
    } catch (error) {
      throw new Error(`Error while fetching recipe with id ${id}: ${error}`);
    }
  }

  // Find a recipe by its id and update its data
  async update(id: string, updateRecipeDto: UpdateRecipeDto) {
    try {
      const recipe = await this.recipeModel.findById(id).exec();
      updateRecipeDto.updatedDate = new Date().toISOString();
      await recipe.updateOne(updateRecipeDto).exec();
      return this.recipeModel.findById(id).exec();
    } catch (error) {
      throw new Error(`Error while updating recipe with id ${id}: ${error}`);
    }
  }

  // Find a recipe by its id and remove it
  async remove(id: string): Promise<RecipeDocument> {
    try {
      return this.recipeModel.findByIdAndDelete(id).exec();
    } catch (error) {
      throw new Error(`Error while removing a recipe with id ${id}: ${error}`);
    }
  }

  async searchRecipes(searchQuery: string): Promise<RecipeDocument[]> {
    const searchResults = await this.recipeModel
      .find({
        $or: [
          { name: { $regex: searchQuery, $options: 'i' } },
          {
            'recipeIngredients.ingredient.name': {
              $regex: searchQuery,
              $options: 'i',
            },
          },
          { category: { $regex: searchQuery, $options: 'i' } },
        ],
      })
      .populate({
        path: 'recipeIngredients',
        model: 'RecipeIngredient',
        populate: {
          path: 'ingredient',
          model: 'Ingredient',
          select: { recipes: 0 },
        },
      })
      .populate({
        path: 'recipeSteps',
        model: 'RecipeStep',
        select: { recipe: 0 },
      })
      .exec();

    return searchResults;
  }
}
