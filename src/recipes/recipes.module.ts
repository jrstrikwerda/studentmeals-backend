import { Module } from '@nestjs/common';
import { RecipesService } from './recipes.service';
import { RecipesController } from './recipes.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Recipe, RecipeSchema } from './entities/recipe.entity';
import {
  RecipeIngredient,
  RecipeIngredientSchema,
} from 'src/recipe-ingredients/entities/recipe-ingredient.entity';
import {
  Ingredient,
  IngredientSchema,
} from 'src/ingredients/entities/ingredient.entity';
import { RecipeIngredientsService } from 'src/recipe-ingredients/recipe-ingredients.service';
import { IngredientsService } from 'src/ingredients/ingredients.service';
import {
  RecipeStep,
  RecipeStepSchema,
} from 'src/recipe-step/entities/recipe-step.entity';
import { RecipeStepsService } from 'src/recipe-step/recipe-steps.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Recipe.name, schema: RecipeSchema }]),
    MongooseModule.forFeature([
      { name: RecipeIngredient.name, schema: RecipeIngredientSchema },
    ]),
    MongooseModule.forFeature([
      { name: Ingredient.name, schema: IngredientSchema },
    ]),
    MongooseModule.forFeature([
      { name: RecipeStep.name, schema: RecipeStepSchema },
    ]),
  ],
  controllers: [RecipesController],
  providers: [
    RecipesService,
    RecipeIngredientsService,
    RecipeStepsService,
    IngredientsService,
  ],
})
export class RecipesModule {}
