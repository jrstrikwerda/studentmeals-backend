import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

export type RecipeDocument = Recipe & Document;

@Schema()
export class Recipe {
  //   @Prop({type: Types.ObjectId, ref: 'Member'})
  //   creator: Member;

  @Prop()
  name: string;

  @Prop()
  description: string;

  @Prop()
  image: string;

  @Prop()
  cookTime: number;

  @Prop()
  creationDate: string;

  @Prop()
  updatedDate: string;

  //   @Prop({type: [Types.ObjectId], ref: 'Categories'})
  //   categories: Category[];

  @Prop({ type: [SchemaTypes.ObjectId], ref: 'RecipeIngredient' })
  recipeIngredients: Types.ObjectId[];

  @Prop({ type: [SchemaTypes.ObjectId], ref: 'RecipeStep' })
  recipeSteps: Types.ObjectId[];
}

export const RecipeSchema = SchemaFactory.createForClass(Recipe);
