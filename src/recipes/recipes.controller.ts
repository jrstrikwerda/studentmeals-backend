import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  NotFoundException,
} from '@nestjs/common';
import { RecipesService } from './recipes.service';
import { CreateRecipeDto } from './dto/create-recipe.dto';
import { UpdateRecipeDto } from './dto/update-recipe.dto';
import { Recipe, RecipeDocument } from './entities/recipe.entity';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';
import { Roles } from 'src/auth/roles/roles.decorator';
import { Role } from 'src/users/dto/user.dto';
// import RoleGuard from 'src/common/guards/role.guard';
import { IdQueryDto } from 'src/common/dtos/id.query.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('recipes')
@Controller('recipes')
export class RecipesController {
  constructor(private readonly recipesService: RecipesService) {}

  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Post()
  create(@Body() createRecipeDto: CreateRecipeDto): Promise<Recipe> {
    return this.recipesService.createRecipe(createRecipeDto);
  }

  @Get()
  findAll(): Promise<Recipe[]> {
    return this.recipesService.findAll();
  }

  @Get('findone/:id')
  async findOne(@Param() params: IdQueryDto): Promise<Recipe> {
    const recipe = await this.recipesService.findOne(params.id);

    if (!recipe) {
      throw new NotFoundException(`recipe with id [${params.id}] not found`);
    }

    return recipe;
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRecipeDto: UpdateRecipeDto,
  ): Promise<Recipe> {
    return this.recipesService.update(id, updateRecipeDto);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Delete(':id')
  remove(@Param('id') id: string): Promise<Recipe> {
    return this.recipesService.remove(id);
  }

  @Get('search')
  async searchRecipes(@Query() query): Promise<RecipeDocument[]> {
    return await this.recipesService.searchRecipes(query?.query);
  }
}
