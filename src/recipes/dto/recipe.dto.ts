import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNumber, IsArray, ValidateNested } from 'class-validator';
import { RecipeIngredientDto } from 'src/recipe-ingredients/dto/recipe-ingredient.dto';
import { RecipeStepDto } from 'src/recipe-step/dto/recipe-step.dto';

// import { MemberDto } from "../member/member.dto";
// import { PostDto } from "../post/post.dto";

export class RecipeDto {
  constructor(partial?: Partial<RecipeDto>) {
    if (partial) {
      Object.assign(this, partial);
    }
  }

  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  description: string;

  @ApiProperty()
  @IsString()
  image: string;

  @ApiProperty()
  @IsNumber()
  cookTime: number;

  @ApiProperty()
  @IsString()
  creationDate: string;

  @ApiProperty()
  @IsString()
  updatedDate: string;

  // categories: CategoryDto[];

  @ApiProperty()
  @ValidateNested()
  @IsArray()
  recipeIngredients: RecipeIngredientDto[];

  @ApiProperty()
  @ValidateNested()
  @IsArray()
  recipeSteps: RecipeStepDto[];
}
