import { RecipeDto } from './recipe.dto';

export class UpdateRecipeDto extends RecipeDto {}
