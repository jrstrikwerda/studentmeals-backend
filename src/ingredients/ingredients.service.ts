import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateIngredientDto } from './dto/create-ingredient.dto';
import { UpdateIngredientDto } from './dto/update-ingredient.dto';
import { Ingredient, IngredientDocument } from './entities/ingredient.entity';

@Injectable()
export class IngredientsService {
  constructor(
    @InjectModel(Ingredient.name)
    private ingredientModel: Model<IngredientDocument>,
  ) {}

  async createIngredient(
    createIngredientDto: CreateIngredientDto,
    recipeId?: Types.ObjectId,
  ): Promise<IngredientDocument> {
    try {
      // Check if an ingredient with the same name already exists
      const existingIngredient = await this.ingredientModel
        .findOne({ name: createIngredientDto.name })
        .exec();

      // If an ingredient with the same name already exists, update it
      if (existingIngredient) {
        // Check if the recipeId was provided
        if (recipeId) {
          // Add the recipeId to the existing ingredient
          existingIngredient.recipes.push(recipeId);
          existingIngredient.save();
        }
        return existingIngredient;
      }

      // Set default values for fields if they are not provided
      createIngredientDto.description = createIngredientDto.description || '';

      // Create a new ingredient
      const newIngredient = await this.ingredientModel.create({
        ...createIngredientDto,
        recipes: [recipeId],
      });

      return newIngredient;
    } catch (error) {
      throw new InternalServerErrorException(
        `Error while creating ingredient: ${error.message}`,
      );
    }
  }

  async findAll(): Promise<IngredientDocument[]> {
    return this.ingredientModel
      .find({})
      .populate({
        path: 'recipes',
        model: 'Recipe',
        select: { name: 1, description: 1 },
      })
      .exec();
  }

  async findOne(id: string): Promise<IngredientDocument> {
    return await this.ingredientModel.findById(id).exec();
  }

  async update(
    id: string,
    updateIngredientDto: UpdateIngredientDto,
  ): Promise<IngredientDocument> {
    const ingredient = await this.ingredientModel.findById(id).exec();
    await ingredient.updateOne(updateIngredientDto).exec();
    return this.ingredientModel.findById(id).exec();
  }

  async updateMany(condition: object, update: object): Promise<void> {
    await this.ingredientModel.updateMany(condition, update);
  }

  async remove(id: string): Promise<IngredientDocument> {
    // const ingredient = await this.ingredientModel.findById(id).exec();
    return this.ingredientModel.findByIdAndDelete(id).exec();
  }
}
