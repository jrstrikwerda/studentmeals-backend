import { IngredientDto } from './ingredient.dto';

export class CreateIngredientDto extends IngredientDto {}
