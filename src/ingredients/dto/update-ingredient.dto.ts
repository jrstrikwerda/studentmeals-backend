import { IngredientDto } from './ingredient.dto';

export class UpdateIngredientDto extends IngredientDto {}
