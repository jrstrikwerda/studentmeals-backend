import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { RecipeDto } from 'src/recipes/dto/recipe.dto';

export class IngredientDto {
  constructor(partial?: Partial<IngredientDto>) {
    if (partial) {
      Object.assign(this, partial);
    }
  }

  @ApiProperty()
  @IsString()
  name: string;

  @ApiPropertyOptional()
  @IsString()
  description?: string;

  //   @ApiProperty()
  //   @IsString()
  //   image: string;

  // ingredientCategories: IngredientCategoryDto[];

  @ApiProperty()
  recipes: RecipeDto[];
}
