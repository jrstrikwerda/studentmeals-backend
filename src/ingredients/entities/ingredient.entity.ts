import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

export type IngredientDocument = Ingredient & Document;

@Schema()
export class Ingredient {
  @Prop()
  name: string;

  @Prop()
  description: string;

  //   @Prop()
  //   image: string;

  //   @Prop({type: [Types.ObjectId], ref: 'IngredientCategory'})
  //   categories: Category[];

  @Prop({ type: [SchemaTypes.ObjectId], ref: 'Recipe' })
  recipes: Types.ObjectId[];
}

export const IngredientSchema = SchemaFactory.createForClass(Ingredient);
