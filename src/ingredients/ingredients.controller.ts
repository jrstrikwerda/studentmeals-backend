import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { IngredientsService } from './ingredients.service';
import { CreateIngredientDto } from './dto/create-ingredient.dto';
import { UpdateIngredientDto } from './dto/update-ingredient.dto';
import { Ingredient } from './entities/ingredient.entity';
import { Roles } from 'src/auth/roles/roles.decorator';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';
// import RoleGuard from 'src/common/guards/role.guard';
import { Role } from 'src/users/dto/user.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
@ApiTags('ingredients')
@Controller('ingredients')
export class IngredientsController {
  constructor(private readonly ingredientsService: IngredientsService) {}

  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Post()
  create(
    @Body() createIngredientDto: CreateIngredientDto,
  ): Promise<Ingredient> {
    return this.ingredientsService.createIngredient(createIngredientDto);
  }

  @Get()
  findAll(): Promise<Ingredient[]> {
    return this.ingredientsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Ingredient> {
    return this.ingredientsService.findOne(id);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateIngredientDto: UpdateIngredientDto,
  ): Promise<Ingredient> {
    return this.ingredientsService.update(id, updateIngredientDto);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Delete(':id')
  remove(@Param('id') id: string): Promise<Ingredient> {
    return this.ingredientsService.remove(id);
  }
}
