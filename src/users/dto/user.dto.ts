import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, Length } from 'class-validator';
// import { Transform } from 'class-transformer';

export enum Role {
  User = 'User',
  Admin = 'Admin',
}

export class UserDto {
  constructor(partial?: Partial<UserDto>) {
    if (partial) {
      Object.assign(this, partial);
    }
  }
  @ApiProperty()
  @IsString()
  // @Transform(ValidatorUtils.transformTrimString)
  email: string;

  @ApiProperty()
  @IsString()
  // @Transform(ValidatorUtils.transformTrimString)
  firstName: string;

  @ApiProperty()
  @IsString()
  // @Transform(ValidatorUtils.transformTrimString)
  lastName: string;

  @ApiProperty()
  @IsString()
  @Length(2, 32)
  // @Transform(ValidatorUtils.transformTrimString)
  @IsOptional()
  username: string;

  @ApiProperty()
  @IsString()
  password: string;

  @ApiProperty()
  @IsString()
  // @Transform(ValidatorUtils.transformTrimString)
  refreshToken: string;

  @ApiProperty()
  role: Role;
}
