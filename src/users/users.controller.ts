import {
  Body,
  Controller,
  Post,
  Get,
  Param,
  UseGuards,
  Req,
  Delete,
  Patch,
  Headers,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDocument } from './entities/user.entity';
import { UpdateUserDto } from './dto/update-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { AccessTokenGuard } from '../common/guards/accessToken.guard';
import RoleGuard from 'src/common/guards/role.guard';
import { Role } from './dto/user.dto';
import { Roles } from 'src/auth/roles/roles.decorator';
import { Request } from 'express';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Get()
  async findAll(): Promise<UserDocument[]> {
    return this.usersService.findAll();
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Get('findone/:id')
  findById(@Param('id') id: string): Promise<UserDocument> {
    return this.usersService.findOneById(id);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<UserDocument> {
    return this.usersService.update(id, updateUserDto);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Roles(Role.Admin)
  @UseGuards(RoleGuard)
  @Delete(':id')
  remove(@Param('id') id: string): Promise<UserDocument> {
    return this.usersService.remove(id);
  }

  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Post('search')
  searchUsers(
    @Req()
    req: Request & {
      user: UserDocument;
    },
  ): Promise<UserDocument[]> {
    return this.usersService.searchUsers(req.user._id);
  }

  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Get('me')
  async getMe(
    @Headers('authorization') accessToken: string,
  ): Promise<UserDocument> {
    const user = this.usersService.findMe(accessToken.slice(7));
    return user;
  }
}
