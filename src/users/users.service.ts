import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument, userFields } from './entities/user.entity';
import { ObjectIdOrString } from '../common/core/types';
import { UserQueries } from 'src/common/queries/users.queries';
import { UpdateUserDto } from './dto/update-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
    private readonly jwtService: JwtService,
  ) {}
  async create(createUserDto: CreateUserDto): Promise<UserDocument> {
    const createdUser = new this.userModel(createUserDto);
    return createdUser.save();
  }

  async findAll(): Promise<UserDocument[]> {
    return this.userModel.find().select(userFields.all).exec();
  }

  async findOneById(id: ObjectIdOrString): Promise<UserDocument> {
    return this.userModel.findById(id).select(userFields.public).exec();
  }

  async findOneByIdPrivate(id: ObjectIdOrString): Promise<UserDocument> {
    return this.userModel.findById(id).select(userFields.all).exec();
  }

  async findOneByUsername(username: string): Promise<UserDocument> {
    return this.userModel
      .findOne({ username })
      .select(userFields.public)
      .exec();
  }

  async findOneByUsernamePrivate(username: string): Promise<UserDocument> {
    return this.userModel.findOne({ username }).select(userFields.all).exec();
  }

  async findOneByEmail(email: string): Promise<UserDocument> {
    return (
      this.userModel
        .findOne({
          userEmail: UserQueries.getEmailFilter(email),
        })
        // .select(userFields.all) // does this require .select and which userfields?
        .exec()
    );
  }

  async update(
    id: ObjectIdOrString,
    updateUserDto: UpdateUserDto,
  ): Promise<UserDocument> {
    return this.userModel
      .findByIdAndUpdate(id, updateUserDto, { new: true })
      .exec();
  }

  async remove(id: ObjectIdOrString): Promise<UserDocument> {
    return this.userModel.findByIdAndDelete(id).exec();
  }

  async searchUsers(currentUserId: ObjectIdOrString): Promise<UserDocument[]> {
    return this.userModel
      .find({
        _id: { $nin: [currentUserId] },
      })
      .select(userFields.public)
      .sort({ _id: -1 })
      .exec();
  }

  async findMe(accessToken: string): Promise<UserDocument> {
    try {
      const decodedToken = this.jwtService.verify(accessToken, {
        secret: process.env.JWT_ACCESS_SECRET,
      });
      const user = await this.findOneById(decodedToken.sub);
      if (!user) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }
      return user;
    } catch (error) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
  }
}
