import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { DbUtils } from 'src/common/utils/db.utils';
import { Role } from '../dto/user.dto';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  firstName: string;

  @Prop({ required: true })
  lastName: string;

  @Prop({ required: true, unique: true })
  username: string;

  @Prop({ required: true, select: false })
  password: string;

  @Prop({ select: false })
  email: string;

  @Prop({ select: false })
  refreshToken: string;

  @Prop({ type: String, enum: Role, default: Role.User })
  role: Role;
}

export const UserSchema = SchemaFactory.createForClass(User);

export const userFields = {
  all: DbUtils.getAllFieldsFromSchema<User>(UserSchema),
  private: DbUtils.getPrivateFieldsFromSchema<User>(UserSchema),
  public: DbUtils.getPublicFieldsFromSchema<User>(UserSchema),
} as const;
