import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RecipesModule } from './recipes/recipes.module';
import { RecipeIngredientsModule } from './recipe-ingredients/recipe-ingredients.module';
import { IngredientsModule } from './ingredients/ingredients.module';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { RecipeStepsModule } from './recipe-step/recipe-steps.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URI),
    RecipesModule,
    RecipeIngredientsModule,
    IngredientsModule,
    UsersModule,
    AuthModule,
    RecipeStepsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
