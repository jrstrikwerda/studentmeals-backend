import { Schema } from 'mongoose';

export class DbUtils {
  public static readonly getAllFieldsFromSchema = <T>(
    schema: Schema,
  ): (keyof T)[] => {
    return Object.entries(schema.paths).map(([key]) => key) as (keyof T)[];
  };

  public static readonly getPublicFieldsFromSchema = <T>(
    schema: Schema,
  ): (keyof T)[] => {
    return (
      Object.entries(schema.paths)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .filter(([_, val]) =>
          [undefined, null, true, 1].includes(val?.options?.select),
        )
        .map(([key]) => key) as (keyof T)[]
    );
  };

  public static readonly getPrivateFieldsFromSchema = <T>(
    schema: Schema,
  ): (keyof T)[] => {
    return (
      Object.entries(schema.paths)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .filter(([_, val]) => val?.options?.select === false)
        .map(([key]) => key) as (keyof T)[]
    );
  };

  public static readonly escapeForRegExp = (string) => {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  };

  /**
   * Convert a string or array into a projection object, retaining all
   * `-` and `+` paths.
   *
   * An example of a use case is converting a Mongoose projection to be used in the
   * aggregate operator `$project`.
   *
   * *Borrowed from Mongoose source, since it doesn't export the function.*
   */
  public static readonly parseProjection = (
    v: any,
    retainMinusPaths = false,
  ) => {
    const type = typeof v;

    if (type === 'string') {
      v = v.split(/\s+/);
    }
    if (
      !Array.isArray(v) &&
      Object.prototype.toString.call(v) !== '[object Arguments]'
    ) {
      return v;
    }

    const len = v.length;
    const ret = {};
    for (let i = 0; i < len; ++i) {
      let field = v[i];
      if (!field) {
        continue;
      }
      const include = '-' == field[0] ? 0 : 1;
      if (!retainMinusPaths && include === 0) {
        field = field.substring(1);
      }
      ret[field] = include;
    }

    return ret;
  };
}
