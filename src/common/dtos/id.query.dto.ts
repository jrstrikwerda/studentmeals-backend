import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId } from 'class-validator';

export class IdQueryDto {
  @ApiProperty()
  @IsMongoId()
  id: string;
}
