import { DbUtils } from '../utils/db.utils';

export class UserQueries {
  public static readonly getEmailFilter = (email: string) => {
    return {
      $exists: true,
      $nin: [null, ''],
      $regex: new RegExp(`^${DbUtils.escapeForRegExp(email)}$`, 'i'),
    };
  };
}
