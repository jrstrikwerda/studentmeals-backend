import { Types } from 'mongoose';

export type ObjectIdOrString = Types.ObjectId | string;
