import { RecipeStepDto } from './recipe-step.dto';

export class UpdateRecipeStepDto extends RecipeStepDto {}
