import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateRecipeStepDto {
  @ApiProperty()
  instruction: string;
  @ApiPropertyOptional()
  image?: string;
  @ApiProperty()
  order: number;
}
