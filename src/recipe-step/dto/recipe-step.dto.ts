import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsString, IsNumber, IsOptional } from 'class-validator';
import { RecipeDto } from 'src/recipes/dto/recipe.dto';

export class RecipeStepDto {
  constructor(partial?: Partial<RecipeStepDto>) {
    if (partial) {
      Object.assign(this, partial);
    }
  }

  @ApiProperty()
  @IsString()
  instruction: string;

  @ApiProperty()
  @IsNumber()
  order: number;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  image?: string;

  @ApiProperty()
  recipe: RecipeDto;
}
