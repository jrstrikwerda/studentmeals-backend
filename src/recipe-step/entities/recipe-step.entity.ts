import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

export type RecipeStepDocument = RecipeStep & Document;

@Schema()
export class RecipeStep {
  @Prop()
  instruction: string;

  @Prop()
  order: number;

  @Prop()
  image?: string;

  @Prop({ type: SchemaTypes.ObjectId, ref: 'Recipe' })
  recipe: Types.ObjectId;
}

export const RecipeStepSchema = SchemaFactory.createForClass(RecipeStep);
