import { Module } from '@nestjs/common';
import { RecipeStepsService } from './recipe-steps.service';
import { RecipeStepsController } from './recipe-steps.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { RecipeStep, RecipeStepSchema } from './entities/recipe-step.entity';
import { RecipesService } from 'src/recipes/recipes.service';
import { Recipe, RecipeSchema } from 'src/recipes/entities/recipe.entity';
import { RecipeIngredientsService } from 'src/recipe-ingredients/recipe-ingredients.service';
import {
  RecipeIngredient,
  RecipeIngredientSchema,
} from 'src/recipe-ingredients/entities/recipe-ingredient.entity';
import {
  Ingredient,
  IngredientSchema,
} from 'src/ingredients/entities/ingredient.entity';
import { IngredientsService } from 'src/ingredients/ingredients.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: RecipeStep.name, schema: RecipeStepSchema },
      { name: Recipe.name, schema: RecipeSchema },
      { name: RecipeIngredient.name, schema: RecipeIngredientSchema },
      { name: Ingredient.name, schema: IngredientSchema },
    ]),
  ],
  controllers: [RecipeStepsController],
  providers: [
    RecipeStepsService,
    RecipesService,
    RecipeIngredientsService,
    IngredientsService,
  ],
})
export class RecipeStepsModule {}
