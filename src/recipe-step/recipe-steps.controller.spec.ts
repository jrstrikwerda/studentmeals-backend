import { Test, TestingModule } from '@nestjs/testing';
import { RecipeStepsController } from './recipe-steps.controller';

describe('RecipeStepController', () => {
  let controller: RecipeStepsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecipeStepsController],
    }).compile();

    controller = module.get<RecipeStepsController>(RecipeStepsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
