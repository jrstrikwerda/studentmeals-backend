import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { RecipeStepsService } from './recipe-steps.service';
import { CreateRecipeStepDto } from './dto/create-recipe-step.dto';
import { UpdateRecipeStepDto } from './dto/update-recipe-step.dto';
import { RecipeStep } from './entities/recipe-step.entity';
import { Roles } from 'src/auth/roles/roles.decorator';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';
// import RoleGuard from 'src/common/guards/role.guard';
import { Role } from 'src/users/dto/user.dto';
import { Types } from 'mongoose';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
@ApiTags('recipe-steps')
@Controller('recipe-steps')
export class RecipeStepsController {
  constructor(private readonly recipeStepsService: RecipeStepsService) {}

  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Post()
  create(
    @Param('recipeId') recipeId: Types.ObjectId,
    @Body() createRecipeStepDto: CreateRecipeStepDto,
  ): Promise<RecipeStep> {
    return this.recipeStepsService.createRecipeStep(
      createRecipeStepDto,
      recipeId,
    );
  }

  @Get()
  findAll(): Promise<RecipeStep[]> {
    return this.recipeStepsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<RecipeStep> {
    return this.recipeStepsService.findOne(id);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRecipeStepDto: UpdateRecipeStepDto,
  ): Promise<RecipeStep> {
    return this.recipeStepsService.update(id, updateRecipeStepDto);
  }

  @Roles(Role.Admin)
  @UseGuards(AccessTokenGuard)
  @ApiBearerAuth()
  @Delete(':id')
  remove(@Param('id') id: string): Promise<RecipeStep> {
    return this.recipeStepsService.remove(id);
  }
}
