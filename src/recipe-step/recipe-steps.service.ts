import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { RecipeStep, RecipeStepDocument } from './entities/recipe-step.entity';
import { CreateRecipeStepDto } from './dto/create-recipe-step.dto';
import { UpdateRecipeStepDto } from './dto/update-recipe-step.dto';

@Injectable()
export class RecipeStepsService {
  constructor(
    @InjectModel(RecipeStep.name)
    private recipeStepModel: Model<RecipeStepDocument>,
  ) {}

  async createRecipeStep(
    createRecipeStepDto: CreateRecipeStepDto,
    recipeId?: Types.ObjectId,
  ): Promise<RecipeStepDocument> {
    try {
      const createdRecipeStep = await this.recipeStepModel.create({
        ...createRecipeStepDto,
        recipe: recipeId,
      });

      return await this.recipeStepModel
        .findById(createdRecipeStep.id)
        .populate({
          path: 'recipe',
          model: 'Recipe',
          select: { recipeSteps: 0 },
        })
        .exec();
    } catch (error) {
      throw new InternalServerErrorException(
        `Error while creating a recipe step: ${error.message}`,
      );
    }
  }

  async findAll(): Promise<RecipeStepDocument[]> {
    return this.recipeStepModel.find({}).exec();
  }

  async findOne(id: string): Promise<RecipeStepDocument> {
    return await this.recipeStepModel.findById(id).exec();
  }

  async update(id: string, updateRecipeStepDto: UpdateRecipeStepDto) {
    const recipeStep = await this.recipeStepModel.findById(id).exec();
    await recipeStep.updateOne(updateRecipeStepDto).exec();
    return this.recipeStepModel.findById(id).exec();
  }

  async updateMany(condition: object, update: object): Promise<void> {
    await this.recipeStepModel.updateMany(condition, update);
  }

  async remove(id: string): Promise<RecipeStepDocument> {
    return this.recipeStepModel.findByIdAndDelete(id).exec();
  }

  //   async createRecipeStep(
  //     recipeId: string,
  //     createRecipeStepDto: CreateRecipeStepDto,
  //   ): Promise<RecipeStepDocument> {
  //     try {
  //       const createdRecipeStep = await this.recipeStepModel.create({
  //         ...createRecipeStepDto,
  //         recipe: recipeId,
  //       });
  //       return createdRecipeStep;
  //     } catch (error) {
  //       throw new Error(`Error while creating a recipe step: ${error}`);
  //     }
  //   }

  //   async findAllByRecipe(recipeId: string): Promise<RecipeStepDocument[]> {
  //     try {
  //       return await this.recipeStepModel
  //         .find({
  //           recipe: recipeId,
  //         })
  //         .exec();
  //     } catch (error) {
  //       throw new Error(`Error while fetching recipe steps: ${error}`);
  //     }
  //   }
}
